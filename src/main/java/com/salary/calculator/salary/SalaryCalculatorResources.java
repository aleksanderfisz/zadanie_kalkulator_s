package com.salary.calculator.salary;


import com.salary.calculator.country.Country;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("calculator")
@Slf4j
public class SalaryCalculatorResources {

    private SalaryCalculator calc;

    public SalaryCalculatorResources(SalaryCalculator calc) {
        this.calc = calc;
    }

    @GetMapping(value = "/calculate/{country}/{grossSalary}", produces = MediaType.APPLICATION_JSON_VALUE)
    public SalaryCalculationRepresentation calculate(@PathVariable String country,
                                                     @PathVariable float grossSalary,
                                                     @RequestParam(name = "provider", required = false) String provider,
                                                     @RequestParam(name = "noCache", required = false,
                                                             defaultValue = "false") boolean noCache)
            throws GetExchangeRateException {
        log.info("Calculating total net salary for country: {}, gross salary per day: {}, provider:{} with cache: {}",
                country, grossSalary, provider, noCache);

        if (BigDecimal.valueOf(grossSalary).compareTo(BigDecimal.ZERO) < 0) {
            log.error("calculating for negative gross salary");
            throw new IllegalArgumentException("Gross salary cannot be negative");
        }

        List<String> countryNames = Arrays.stream(Country.values()).map(Country::name).collect(Collectors.toList());

        if (!countryNames.contains(country)) {
            log.error("Calculating for not supported country");
            throw new IllegalArgumentException("Not supported country");
        }

        Country countryEnum = Country.valueOf(country);


        SalaryCalculationRepresentation result = calc.getNetSalary(countryEnum, grossSalary, provider, noCache);

        log.info("Calculated total salary is: {}", result);

        return result;
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(GetExchangeRateException.class)
    public ErrorEntity getExchangeRateError(GetExchangeRateException err) {
        log.error("Error getting currency: {} from provider {} : {}", err.getCurrency(), err.getProvider(),
                err.getMessage());
        return new ErrorEntity("Error getting currency:" + err.getCurrency() + " form provider " + err.getProvider() + " : "
                + err.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public ErrorEntity getIllegalArgumentError(IllegalArgumentException err) {
        log.error("Illegal argument error: {}", err.getMessage());
        return new ErrorEntity(err.getMessage());
    }
}
