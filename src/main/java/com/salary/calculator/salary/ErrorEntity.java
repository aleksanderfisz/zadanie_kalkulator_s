package com.salary.calculator.salary;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class ErrorEntity {
    private String message;
}