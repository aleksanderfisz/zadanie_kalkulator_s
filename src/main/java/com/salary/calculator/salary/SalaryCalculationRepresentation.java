package com.salary.calculator.salary;

import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
class SalaryCalculationRepresentation {
    private float netSalary;
    private CurrencyRepresentation currency;
}
