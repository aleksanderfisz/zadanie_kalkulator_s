package com.salary.calculator.salary;

import com.salary.calculator.country.Country;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;

public interface SalaryCalculator {
    SalaryCalculationRepresentation getNetSalary(Country country, float grossSalary, String providerName, boolean noCache) throws GetExchangeRateException;
}
