package com.salary.calculator.salary;

import com.salary.calculator.country.Country;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import com.salary.calculator.exchangeRate.ExchangeRateService;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Slf4j
class SalaryCalculatorImpl implements SalaryCalculator {
    private final static BigDecimal WORK_DAYS = new BigDecimal(22);

    private ExchangeRateService service;

    public SalaryCalculatorImpl(ExchangeRateService service) {
        this.service = service;
    }

    @Override
    public SalaryCalculationRepresentation getNetSalary(Country country, float grossSalary, String providerName, boolean noCache)
            throws GetExchangeRateException {
        log.debug("Calculating salary for county: {} gross salary: {}, provider: {}, with cache: {}",
                country, grossSalary, providerName, noCache);

        CurrencyRepresentation rep = service.getExchangeRate(country.getCurrency(), providerName, noCache);

        BigDecimal totalAmount = getTotalAmountEarned(grossSalary, rep.getValue(), country.getFixedCosts());

        BigDecimal tax = getTax(totalAmount, country.getTaxRate());

        float netSalary = totalAmount.subtract(tax).floatValue();

        return SalaryCalculationRepresentation.builder().netSalary(netSalary).currency(rep).build();
    }

    private BigDecimal getTotalAmountEarned(float grossSalary, float exchangeRate, int fixedCost) {
        BigDecimal salary = new BigDecimal(grossSalary).multiply(WORK_DAYS);
        BigDecimal rate = new BigDecimal(exchangeRate);
        BigDecimal cost = new BigDecimal(fixedCost);

        return salary.multiply(rate).subtract(cost.multiply(rate));
    }

    private BigDecimal getTax(BigDecimal totalAmount, int taxRate) {
        if (totalAmount.compareTo(BigDecimal.ZERO) <= 0) {
            log.debug("Negative or zero salary is not taxed");
            return BigDecimal.ZERO;
        }

        BigDecimal hundred = new BigDecimal(100);
        BigDecimal rate = new BigDecimal(taxRate);
        BigDecimal rateInPercentage = rate.divide(hundred);

        return totalAmount.multiply(rateInPercentage);
    }

}
