package com.salary.calculator.country;

import com.salary.calculator.exchangeRate.Currency;
import lombok.Getter;

import static com.salary.calculator.exchangeRate.Currency.*;

@Getter
public enum Country {
    PL(19, 1200, PLN),
    UK(25, 600, GBP),
    DE(20, 800, EUR);

    private int taxRate;
    private int fixedCosts;
    private Currency currency;

    Country(int taxRate, int fixedCosts, Currency currency) {
        this.taxRate = taxRate;
        this.fixedCosts = fixedCosts;
        this.currency = currency;
    }
}
