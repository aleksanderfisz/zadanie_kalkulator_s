package com.salary.calculator.country;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("country")
public class CountryResources {

    @GetMapping(value = "getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Country[] getAllCountries() {
        return Country.values();
    }
}
