package com.salary.calculator.exchangeRate.cache.impl;

import com.salary.calculator.exchangeRate.Currency;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import com.salary.calculator.exchangeRate.cache.ExchangeRateCacheInterface;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
class ExchangeRateCache implements ExchangeRateCacheInterface {
    private Map<ExchangeRateCacheKey, ExchangeRateCacheEntry> exchangeRateCache;

    public ExchangeRateCache() {
        this.exchangeRateCache = new HashMap<>();
    }

    public void addNewExchangeRate(CurrencyRepresentation representation) {
        if (representation.getCurr() == null || representation.getObtainedAt() == null) {
            log.error("Adding new exchange rate without currency or obtain date");
            throw new NullPointerException("Currency and obtained date can't be null");
        }

        if (Currency.PLN == representation.getCurr()) {
            log.error("Adding PLN exchange rate");
            throw new IllegalArgumentException("Cannot create exchange rate from PLN to PLN");
        }

        if (representation.getValue() <= 0.0) {
            log.error("Adding exchange with negative or zero value");
            throw new IllegalArgumentException("Exchange rate must be more then 0");
        }

        if (StringUtils.isBlank(representation.getProvider())) {
            log.error("Adding exchange rate without provider");
            throw new IllegalArgumentException("Provider name can't be blank");
        }

        log.debug("Adding exchange rate: {}", representation);

        ExchangeRateCacheKey key = ExchangeRateCacheKey.builder()
                .curr(representation.getCurr()).provider(representation.getProvider()).build();
        ExchangeRateCacheEntry entry = ExchangeRateCacheEntry.builder()
                .value(representation.getValue()).saveDate(representation.getObtainedAt()).build();
        exchangeRateCache.put(key, entry);
    }

    public CurrencyRepresentation getExchangeRate(Currency curr, String provider) {
        log.debug("Getting exchange rate from cache for currency: {} and provider: {}", curr, provider);

        ExchangeRateCacheEntry entry = exchangeRateCache.get(ExchangeRateCacheKey
                .builder().curr(curr).provider(provider).build());

        if (entry != null) {
            return CurrencyRepresentation.builder()
                    .curr(curr)
                    .provider(provider)
                    .obtainedAt(entry.getSaveDate())
                    .value(entry.getValue())
                    .build();
        }

        return null;
    }
}
