package com.salary.calculator.exchangeRate.cache;

import com.salary.calculator.exchangeRate.Currency;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;

public interface ExchangeRateCacheInterface {
    void addNewExchangeRate(CurrencyRepresentation representation);

    CurrencyRepresentation getExchangeRate(Currency curr, String provider);
}
