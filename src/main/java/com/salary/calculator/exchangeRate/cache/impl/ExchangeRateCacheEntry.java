package com.salary.calculator.exchangeRate.cache.impl;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.Instant;

@Data
@Builder
@ToString
class ExchangeRateCacheEntry {
    private Instant saveDate;
    private float value;
}
