package com.salary.calculator.exchangeRate.cache.impl;

import com.salary.calculator.exchangeRate.Currency;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
class ExchangeRateCacheKey {
    private Currency curr;
    private String provider;
}
