package com.salary.calculator.exchangeRate;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.Instant;

@Data
@Builder
@ToString
public class CurrencyRepresentation {
    private float value;
    private Instant obtainedAt;
    private Currency curr;
    private String provider;

    public static CurrencyRepresentation createPLNRepresentation() {
        return CurrencyRepresentation.builder()
                .curr(Currency.PLN)
                .value(1.0f)
                .obtainedAt(null)
                .provider(null)
                .build();
    }
}
