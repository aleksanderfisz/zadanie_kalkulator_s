package com.salary.calculator.exchangeRate;

public enum Currency {
    PLN,
    EUR,
    GBP
}
