package com.salary.calculator.exchangeRate;

import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;

import java.util.List;

public interface ExchangeRateService {
    CurrencyRepresentation getExchangeRate(Currency currency, String provider, boolean noCache) throws GetExchangeRateException;

    List<String> getProvidersNames();
}
