package com.salary.calculator.exchangeRate.scheduler;

import com.salary.calculator.exchangeRate.Currency;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import com.salary.calculator.exchangeRate.cache.ExchangeRateCacheInterface;
import com.salary.calculator.exchangeRate.provider.ExchangeRateProvider;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
@Slf4j
class ExchangeRateScheduler {

    private ExchangeRateCacheInterface cache;
    private List<ExchangeRateProvider> providers;

    @Value("${exchange.rate.cache.enable:true}")
    private boolean enable;

    public ExchangeRateScheduler(ExchangeRateCacheInterface cache, List<ExchangeRateProvider> providers) {
        this.cache = cache;
        this.providers = providers;
    }

    @Scheduled(fixedDelayString = "${exchange.rate.cache.refresh.second:60}000")
    public void refreshCache() {
        if (enable) {
            log.info("Exchange rate cache refresh start");

            Arrays.stream(Currency.values()).filter(curr -> Currency.PLN != curr).forEach(this::refreshCurrency);
        }
    }

    private void refreshCurrency(Currency currency) {
        providers.stream()
                .map(prov -> getExchangeRate(prov, currency))
                .filter(Objects::nonNull)
                .forEach(rep -> cache.addNewExchangeRate(rep));
    }

    private CurrencyRepresentation getExchangeRate(ExchangeRateProvider provider, Currency currency) {
        try {
            return provider.getExchangeRate(currency);
        } catch (GetExchangeRateException e) {
            log.error("Error getting {} exchange rate from {}: {}", e.getCurrency(), e.getProvider(), e.getMessage());
            return null;
        }
    }

    protected void setEnableRefresh(boolean enable) {
        this.enable = enable;
    }
}
