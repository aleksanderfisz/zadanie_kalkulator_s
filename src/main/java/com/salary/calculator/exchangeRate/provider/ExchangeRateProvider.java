package com.salary.calculator.exchangeRate.provider;

import com.salary.calculator.exchangeRate.Currency;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;

public abstract class ExchangeRateProvider {

    private RestTemplate restTemplate;

    public ExchangeRateProvider(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CurrencyRepresentation getExchangeRate(Currency currency) throws GetExchangeRateException {
        return CurrencyRepresentation.builder()
                .curr(currency)
                .value(getValue(currency))
                .provider(getProviderName())
                .obtainedAt(Instant.now())
                .build();
    }

    public abstract String getProviderName();

    protected abstract float getValue(Currency currency) throws GetExchangeRateException;

    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }
}
