package com.salary.calculator.exchangeRate.provider.nbp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
class NBPRate {
    private String no;
    private Date effectiveDate;
    private float mid;
}
