package com.salary.calculator.exchangeRate.provider.ecb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.Map;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
class ECBExchangeRateResponse {
    private String base;
    private Date date;
    private Map<String, Float> rates;
}
