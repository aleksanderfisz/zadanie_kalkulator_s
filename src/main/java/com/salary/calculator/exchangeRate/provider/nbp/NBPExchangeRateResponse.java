package com.salary.calculator.exchangeRate.provider.nbp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
class NBPExchangeRateResponse {
    private String table;
    private String currency;
    private String code;
    private List<NBPRate> rates;
}
