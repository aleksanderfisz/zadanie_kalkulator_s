package com.salary.calculator.exchangeRate.provider.nbp;

import com.salary.calculator.exchangeRate.Currency;
import com.salary.calculator.exchangeRate.provider.ExchangeRateProvider;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Profile("!TEST")
@Slf4j
public class NBPExchangeRateProvider extends ExchangeRateProvider {
    private static final String PROVIDER_NAME = "NBP";


    public NBPExchangeRateProvider(RestTemplate restTemplate) {
        super(restTemplate);
    }

    @Override
    protected float getValue(Currency currency) throws GetExchangeRateException {
        NBPExchangeRateResponse rate = getRate(currency);

        if (rate.getRates() == null || rate.getRates().isEmpty()) {
            log.error("Empty rates for {} from provider: {}", currency, PROVIDER_NAME);
            throw new GetExchangeRateException(currency, PROVIDER_NAME,
                    "Empty rates");
        }

        return rate.getRates().get(0).getMid();
    }

    @Override
    public String getProviderName() {
        return PROVIDER_NAME;
    }

    protected NBPExchangeRateResponse getRate(Currency curr) throws GetExchangeRateException {
        log.debug("Getting: {} from provider: {}", curr, PROVIDER_NAME);

        ResponseEntity<NBPExchangeRateResponse> response;
        try {
            response = getRestTemplate().getForEntity(getNbpRatesUrl(curr),
                    NBPExchangeRateResponse.class);
        } catch (Exception e) {
            log.error("Error calling provider: {}", PROVIDER_NAME);
            throw new GetExchangeRateException(curr, PROVIDER_NAME,
                    "Error calling provider: " + e.getMessage());
        }

        if (response.getStatusCode() != HttpStatus.OK) {
            log.error("Invalid response code fro: {} from: {}", curr, PROVIDER_NAME);
            throw new GetExchangeRateException(curr, PROVIDER_NAME,
                    "invalid response code: " + response.getStatusCode().name());
        }

        return response.getBody();
    }

    protected String getNbpRatesUrl(Currency curr) {
        return "http://api.nbp.pl/api/exchangerates/rates/a/" + curr.name().toLowerCase();
    }
}
