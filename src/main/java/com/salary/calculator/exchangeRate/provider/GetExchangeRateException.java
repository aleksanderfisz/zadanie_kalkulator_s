package com.salary.calculator.exchangeRate.provider;

import com.salary.calculator.exchangeRate.Currency;
import lombok.Getter;

@Getter
public class GetExchangeRateException extends Exception {
    private final Currency currency;
    private final String provider;

    public GetExchangeRateException(Currency currency, String provider, String message) {
        super(message);
        this.currency = currency;
        this.provider = provider;
    }
}
