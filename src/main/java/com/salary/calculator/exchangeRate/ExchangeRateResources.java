package com.salary.calculator.exchangeRate;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("exchangeRate")
public class ExchangeRateResources {
    private ExchangeRateService service;

    public ExchangeRateResources(ExchangeRateService service) {
        this.service = service;
    }

    @GetMapping(value = "/providers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getAllProviders() {
        return service.getProvidersNames();
    }
}
