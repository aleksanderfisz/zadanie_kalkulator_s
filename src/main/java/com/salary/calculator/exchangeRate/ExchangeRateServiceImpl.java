package com.salary.calculator.exchangeRate;

import com.salary.calculator.exchangeRate.cache.ExchangeRateCacheInterface;
import com.salary.calculator.exchangeRate.provider.ExchangeRateProvider;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
class ExchangeRateServiceImpl implements ExchangeRateService {

    private List<ExchangeRateProvider> providers;
    private ExchangeRateCacheInterface cache;

    public ExchangeRateServiceImpl(List<ExchangeRateProvider> providers, ExchangeRateCacheInterface cache) {
        this.providers = providers;
        this.cache = cache;
    }

    @Override
    public CurrencyRepresentation getExchangeRate(Currency currency, String provider, boolean noCache) throws GetExchangeRateException {
        if (Currency.PLN == currency) {
            return CurrencyRepresentation.createPLNRepresentation();
        }

        if (currency == null) {
            log.error("Getting exchange rate for null currency");
            throw new NullPointerException("Currency can't be null");
        }

        if (StringUtils.isNotBlank(provider) && !isProviderSupported(provider)) {
            log.error("Getting exchange rate for not supported provider");
            throw new IllegalArgumentException("Not supported provider");
        }

        CurrencyRepresentation currencyRepresentation = null;

        if (!noCache) {
            log.debug("Getting currency: {} provider: {} from cache", currency, provider);
            currencyRepresentation = getExchangeRateFromCache(currency, provider);
        }

        if (currencyRepresentation == null) {
            log.debug("Getting currency: {} provider: {} from provider", currency, provider);
            currencyRepresentation = getExchangeRateFromProvider(currency, provider);
        }

        return currencyRepresentation;
    }

    @Override
    public List<String> getProvidersNames() {
        return providers.stream().map(ExchangeRateProvider::getProviderName).collect(Collectors.toList());
    }

    private CurrencyRepresentation getExchangeRateFromCache(Currency currency, String provider) {
        if (StringUtils.isNotBlank(provider)) {
            return cache.getExchangeRate(currency, provider);
        }

        CurrencyRepresentation representation = null;
        for (ExchangeRateProvider prov : providers) {
            representation = cache.getExchangeRate(currency, prov.getProviderName());
            if (representation != null) {
                break;
            }
        }

        return representation;
    }

    private CurrencyRepresentation getExchangeRateFromProvider(Currency currency, String provider)
            throws GetExchangeRateException {
        if (StringUtils.isNotBlank(provider)) {
            ExchangeRateProvider exchangeRateProvider = providers.stream()
                    .filter(prov -> prov.getProviderName().equals(provider)).findAny().get();

            return exchangeRateProvider.getExchangeRate(currency);
        }

        StringBuilder providersNames = new StringBuilder();
        CurrencyRepresentation representation = null;
        for (ExchangeRateProvider prov : providers) {
            try {
                representation = prov.getExchangeRate(currency);
            } catch (GetExchangeRateException e) {
                providersNames.append(prov.getProviderName()).append(",");
            }
            if (representation != null) {
                break;
            }
        }

        if (representation == null) {
            log.error("All of providers failed to get exchange rate for: {}", currency);
            throw new GetExchangeRateException(currency, providersNames.toString(),
                    "Error getting exchange rate from multiple provicers");
        }

        return representation;
    }

    private boolean isProviderSupported(String provider) {
        return providers.stream().anyMatch(prov -> prov.getProviderName().equals(provider));
    }
}
