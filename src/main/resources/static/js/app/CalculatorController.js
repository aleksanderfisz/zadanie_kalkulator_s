'use strict'

var module = angular.module('calc.controller', []);
module.controller("CalcController", ["$scope", "CalcService",
    function($scope, CalcService) {
        $scope.currency = {
            country: null,
            grossSalary: 0,
            providerName: null,
            noCache: false
        };

        $scope.clear = function (){
            $scope.currency = {
                    country: $scope.countries[0],
                    grossSalary: 0,
                    providerName: null,
                    noCache: false
                };
        }

        $scope.countries = [];
        CalcService.getCountries().then(function(value) {
            $scope.countries = value.data;
            $scope.currency.country = $scope.countries[0];
        }, function(reason) {
            console.log("error occured");
        }, function(value) {
            console.log("no callback");
        });

        $scope.providers = [];
        CalcService.getProviders().then(function(value) {
            $scope.providers = value.data;
        }, function(reason) {
            console.log("error occured");
        }, function(value) {
            console.log("no callback");
        });

        $scope.calcResult = null;
        $scope.resultError = null;
        $scope.calcSalary = function() {
            CalcService.calculate($scope.currency).then(function(value) {
                $scope.calcResult = value.data;
                $scope.resultError = null;
            }, function(data) {
                $scope.calcResult = null;
                $scope.resultError = data.data.message;
                console.log("error occured");
            });
        }
    }
]);