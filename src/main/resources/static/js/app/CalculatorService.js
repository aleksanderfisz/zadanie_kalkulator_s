'use strict'
angular.module('calc.service', []).factory('CalcService', ["$http", "CONSTANTS", function($http, CONSTANTS) {
    var service = {};
    service.getCountries = function() {
        return $http.get(CONSTANTS.getCountries);
    }
    service.getProviders = function() {
        return $http.get(CONSTANTS.getProviders);
    }
    service.calculate = function(currency) {
        var country  = currency.country;
        var grossSalary = currency.grossSalary;
        var provider = currency.providerName;
        var noCache = currency.noCache;

        var params = {};
        if (!!provider){
            params["provider"]=provider
        }

        if(!!noCache){
            params["noCache"]=noCache
        }

        var url = CONSTANTS.getCalculator + "/" + country + "/" + grossSalary
        return $http.get(url, {params : params});
    }
    return service;
}]);