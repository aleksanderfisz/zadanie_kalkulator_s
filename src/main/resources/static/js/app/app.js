'use strict'

var calcApp = angular.module('calc', ['ui.bootstrap', 'calc.controller', 'calc.service']);
calcApp.constant("CONSTANTS", {
    getCountries: "/country/getAll",
    getProviders: "/exchangeRate/providers",
    getCalculator: "/calculator/calculate"
});