package com.salary.calculator.salary;

import com.salary.calculator.CurrencyRepresentationFactory;
import com.salary.calculator.country.Country;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import com.salary.calculator.exchangeRate.ExchangeRateService;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.salary.calculator.exchangeRate.Currency.EUR;
import static com.salary.calculator.exchangeRate.Currency.PLN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class SalaryCalculatorTests {

    private static final int GROSS_SALARY = 100;
    private static final int MIN_GROSS_SALARY1 = 10;

    @Mock
    private ExchangeRateService service;

    private SalaryCalculator calculator;

    @Before
    public void setup() {
        calculator = new SalaryCalculatorImpl(service);
    }

    @Test
    public void getNetSalaryShouldReturnNetSalary() throws GetExchangeRateException {
        CurrencyRepresentation rep = CurrencyRepresentation.createPLNRepresentation();

        doReturn(rep).when(service).getExchangeRate(PLN, null, false);

        SalaryCalculationRepresentation result = calculator
                .getNetSalary(Country.PL, GROSS_SALARY, null, false);

        assertThat(BigDecimal.valueOf(result.getNetSalary())).isEqualTo(BigDecimal.valueOf(810f));
        assertThat(result.getCurrency()).isEqualTo(rep);
    }

    @Test
    public void getNetSalaryShouldNotTaxOnNegativeGrossTotal() throws GetExchangeRateException {
        CurrencyRepresentation rep = CurrencyRepresentation.createPLNRepresentation();

        doReturn(rep).when(service).getExchangeRate(PLN, null, false);

        SalaryCalculationRepresentation result = calculator
                .getNetSalary(Country.PL, MIN_GROSS_SALARY1, null, false);

        assertThat(BigDecimal.valueOf(result.getNetSalary())).isEqualTo(BigDecimal.valueOf(-980f));
        assertThat(result.getCurrency()).isEqualTo(rep);
    }

    @Test
    public void getNSalaryShouldReturnNetSalaryForEuro() throws GetExchangeRateException {
        CurrencyRepresentation rep = CurrencyRepresentationFactory.createDefault();

        doReturn(rep).when(service).getExchangeRate(EUR, null, false);

        SalaryCalculationRepresentation result = calculator
                .getNetSalary(Country.DE, GROSS_SALARY, null, false);

        assertThat(BigDecimal.valueOf(result.getNetSalary())).isEqualTo(BigDecimal.valueOf(2240f));
        assertThat(result.getCurrency()).isEqualTo(rep);
    }
}
