package com.salary.calculator.salary;

import com.salary.calculator.CurrencyRepresentationFactory;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import com.salary.calculator.exchangeRate.cache.ExchangeRateCacheInterface;
import com.salary.calculator.exchangeRate.provider.ExchangeRateProvider;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;

import static com.salary.calculator.exchangeRate.Currency.EUR;
import static com.salary.calculator.exchangeRate.Currency.GBP;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
public class SalaryCalculatorResourcesTests {

    private static final String ERROR_MSG = "Error";
    private static final String PROVIDER_3_NAME = "Provider3";
    private static final String PROVIDER_1_NAME = "Provider1";
    private static final String PROVIDER_2_NAME = "Provider2";
    private static final Instant OBTAINED_AT = Instant.now();
    private static final float EURO_VALUE_1 = 4.0f;
    private static final float GBP_VALUE_1 = 5.0f;
    private static final float EURO_VALUE_2 = 4.5f;
    private static final float GBP_VALUE_2 = 5.5f;
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ExchangeRateCacheInterface cache;

    @MockBean(name = "provider1")
    private ExchangeRateProvider provider1;

    @MockBean(name = "provider2")
    private ExchangeRateProvider provider2;

    @Before
    public void setup() throws GetExchangeRateException {
        CurrencyRepresentation prov1EuroRep = CurrencyRepresentation.builder()
                .curr(EUR)
                .provider(PROVIDER_1_NAME)
                .value(EURO_VALUE_1)
                .obtainedAt(OBTAINED_AT)
                .build();

        CurrencyRepresentation prov1GbpRep = CurrencyRepresentation.builder()
                .curr(GBP)
                .provider(PROVIDER_1_NAME)
                .value(GBP_VALUE_1)
                .obtainedAt(OBTAINED_AT)
                .build();

        CurrencyRepresentation provider2EuroRep = CurrencyRepresentation.builder()
                .curr(EUR)
                .provider(PROVIDER_2_NAME)
                .value(EURO_VALUE_2)
                .obtainedAt(OBTAINED_AT)
                .build();

        CurrencyRepresentation provider2GbpRep = CurrencyRepresentation.builder()
                .curr(GBP)
                .provider(PROVIDER_2_NAME)
                .value(GBP_VALUE_2)
                .obtainedAt(OBTAINED_AT)
                .build();

        CurrencyRepresentation cacheRep = CurrencyRepresentationFactory
                .createWithCurrencyAndProvider(EUR, PROVIDER_1_NAME);

        doReturn(prov1EuroRep).when(provider1).getExchangeRate(EUR);
        doReturn(prov1GbpRep).when(provider1).getExchangeRate(GBP);
        doReturn(PROVIDER_1_NAME).when(provider1).getProviderName();

        doReturn(provider2EuroRep).when(provider2).getExchangeRate(EUR);
        doReturn(provider2GbpRep).when(provider2).getExchangeRate(GBP);
        doReturn(PROVIDER_2_NAME).when(provider2).getProviderName();

        cache.addNewExchangeRate(cacheRep);
    }

    @Test
    public void calculateShouldReturnForNoProvider() throws Exception {
        mvc.perform(get("/calculator/calculate/UK/100"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.netSalary", is(6000.0)))
                .andExpect(jsonPath("$.currency.curr", is("GBP")));
    }

    @Test
    public void calculateShouldReturnForProvider() throws Exception {
        mvc.perform(get("/calculator/calculate/UK/100").param("provider", PROVIDER_2_NAME))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.netSalary", is(6600.0)))
                .andExpect(jsonPath("$.currency.curr", is("GBP")))
                .andExpect(jsonPath("$.currency.provider", is(PROVIDER_2_NAME)));
    }

    @Test
    public void calculateShouldReturnForNoCacheAndNoProvider() throws Exception {
        mvc.perform(get("/calculator/calculate/UK/100").param("noCache", "true"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.netSalary", is(6000.0)))
                .andExpect(jsonPath("$.currency.curr", is("GBP")));
    }

    @Test
    public void calculateShouldReturnFromCacheForNoProvider() throws Exception {
        mvc.perform(get("/calculator/calculate/DE/100"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.netSalary", is(2240.0)))
                .andExpect(jsonPath("$.currency.curr", is("EUR")))
                .andExpect(jsonPath("$.currency.provider", is(PROVIDER_1_NAME)));
    }

    @Test
    public void calculateShouldReturnNotFromCacheForProvider() throws Exception {
        mvc.perform(get("/calculator/calculate/DE/100").param("provider", PROVIDER_2_NAME))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.netSalary", is(5040.0)))
                .andExpect(jsonPath("$.currency.curr", is("EUR")))
                .andExpect(jsonPath("$.currency.provider", is(PROVIDER_2_NAME)));
    }

    @Test
    public void calculateShouldReturnNotFromCacheForNoProviderAndNoCache() throws Exception {
        mvc.perform(get("/calculator/calculate/DE/100").param("noCache", "true"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.netSalary", is(4480.0)))
                .andExpect(jsonPath("$.currency.curr", is("EUR")))
                .andExpect(jsonPath("$.currency.provider", is(PROVIDER_1_NAME)));
    }

    @Test
    public void calculateShouldReturnBadRequestForBrokenProvider() throws Exception {
        doThrow(new GetExchangeRateException(GBP, PROVIDER_1_NAME, ERROR_MSG)).when(provider1).getExchangeRate(GBP);

        String errorMessage = "Error getting currency:" + GBP.name() + " form provider " + PROVIDER_1_NAME + " : " + ERROR_MSG;

        mvc.perform(get("/calculator/calculate/UK/100").param("provider", PROVIDER_1_NAME))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is(errorMessage)));
    }

    @Test
    public void calculateShouldReturnBadRequestForAllProvidersBrokenAndNoSpecifiedProvider() throws Exception {
        doThrow(new GetExchangeRateException(GBP, PROVIDER_1_NAME, ERROR_MSG)).when(provider1).getExchangeRate(GBP);
        doThrow(new GetExchangeRateException(GBP, PROVIDER_2_NAME, ERROR_MSG)).when(provider2).getExchangeRate(GBP);


        String errorMessage = "Error getting currency:" + GBP.name() + " form provider " + PROVIDER_1_NAME + "," +
                PROVIDER_2_NAME + ", : Error getting exchange rate from multiple provicers";

        mvc.perform(get("/calculator/calculate/UK/100"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is(errorMessage)));
    }

    @Test
    public void calculateShouldThrowOnNegativeGrossSalary() throws Exception {
        mvc.perform(get("/calculator/calculate/UK/-100"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Gross salary cannot be negative")));
    }

    @Test
    public void calculateShouldThrowOnNotSupportedCountry() throws Exception {
        mvc.perform(get("/calculator/calculate/UZ/100"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Not supported country")));
    }

    @Test
    public void calculateShouldThrowOnNotSupportedProvider() throws Exception {
        mvc.perform(get("/calculator/calculate/UK/100").param("provider", PROVIDER_3_NAME))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Not supported provider")));
    }
}
