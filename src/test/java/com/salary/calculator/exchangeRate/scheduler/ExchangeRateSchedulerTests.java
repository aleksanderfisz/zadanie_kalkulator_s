package com.salary.calculator.exchangeRate.scheduler;

import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import com.salary.calculator.exchangeRate.cache.ExchangeRateCacheInterface;
import com.salary.calculator.exchangeRate.provider.ExchangeRateProvider;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static com.salary.calculator.CurrencyRepresentationFactory.createWithCurrencyAndProvider;
import static com.salary.calculator.exchangeRate.Currency.EUR;
import static com.salary.calculator.exchangeRate.Currency.GBP;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateSchedulerTests {

    private static final String PROVIDER_1_NAME = "Provider1";
    private static final String PROVIDER_2_NAME = "Provider2";
    private static final String PROVIDER_CALLL_ERROR_MESSAGE = "Error calling provider";

    @Mock
    private ExchangeRateProvider provider1;

    @Mock
    private ExchangeRateProvider provider2;

    @Mock
    private ExchangeRateCacheInterface cache;

    private ExchangeRateScheduler scheduler;

    @Before
    public void setup() {
        scheduler = new ExchangeRateScheduler(cache, Arrays.asList(provider1, provider2));
        scheduler.setEnableRefresh(true);
    }


    @Test
    public void refreshCacheShouldAddCurrenciesToCache() throws GetExchangeRateException {
        CurrencyRepresentation provider1EurRep = createWithCurrencyAndProvider(EUR, PROVIDER_1_NAME);
        CurrencyRepresentation provider1GbpRep = createWithCurrencyAndProvider(GBP, PROVIDER_1_NAME);
        CurrencyRepresentation provider2EurRep = createWithCurrencyAndProvider(EUR, PROVIDER_2_NAME);
        CurrencyRepresentation provider2GbpRep = createWithCurrencyAndProvider(GBP, PROVIDER_2_NAME);

        doReturn(provider1EurRep).when(provider1).getExchangeRate(EUR);
        doReturn(provider1GbpRep).when(provider1).getExchangeRate(GBP);

        doReturn(provider2EurRep).when(provider2).getExchangeRate(EUR);
        doReturn(provider2GbpRep).when(provider2).getExchangeRate(GBP);

        scheduler.refreshCache();

        verify(cache).addNewExchangeRate(provider1EurRep);
        verify(cache).addNewExchangeRate(provider1GbpRep);
        verify(cache).addNewExchangeRate(provider2EurRep);
        verify(cache).addNewExchangeRate(provider2GbpRep);
    }

    @Test
    public void refreshCacheShouldSkipBrokenExchangeRate() throws GetExchangeRateException {
        CurrencyRepresentation provider1EurRep = createWithCurrencyAndProvider(EUR, PROVIDER_1_NAME);
        CurrencyRepresentation provider2EurRep = createWithCurrencyAndProvider(EUR, PROVIDER_2_NAME);


        doReturn(provider1EurRep).when(provider1).getExchangeRate(EUR);

        doReturn(provider2EurRep).when(provider2).getExchangeRate(EUR);

        doThrow(new GetExchangeRateException(GBP, PROVIDER_2_NAME, PROVIDER_CALLL_ERROR_MESSAGE))
                .when(provider2)
                .getExchangeRate(GBP);

        scheduler.refreshCache();

        verify(cache, times(2)).addNewExchangeRate(any());
    }

}
