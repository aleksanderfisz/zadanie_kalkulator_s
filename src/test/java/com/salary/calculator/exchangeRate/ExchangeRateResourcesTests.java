package com.salary.calculator.exchangeRate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ComponentScan(basePackageClasses = ExchangeRateResources.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ExchangeRateResourcesTests {

    private static final String PROVIDER_1 = "Provider1";
    private static final String PROVIDER_2 = "provider2";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ExchangeRateService service;

    @Test
    public void getAllProvidersShouldReturnProviders() throws Exception {
        List<String> providers = Arrays.asList(PROVIDER_1, PROVIDER_2);

        doReturn(providers).when(service).getProvidersNames();

        mvc.perform(get("/exchangeRate/providers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(providers.size())))
                .andExpect(jsonPath("$[0]", is(PROVIDER_1)));
    }
}
