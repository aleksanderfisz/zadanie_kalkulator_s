package com.salary.calculator.exchangeRate.cache.impl;

import com.salary.calculator.exchangeRate.Currency;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import org.junit.Test;

import java.time.temporal.ChronoUnit;

import static com.salary.calculator.CurrencyRepresentationFactory.*;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class ExchangeRateCacheTests {

    ExchangeRateCache cache = new ExchangeRateCache();

    @Test(expected = IllegalArgumentException.class)
    public void addNewExchangeRateShouldThrowExceptionOnNegativeValue() {
        CurrencyRepresentation representation = createWithValue(-1.0f);
        cache.addNewExchangeRate(representation);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addNewExchangeRateShouldThrowExceptionOnZeroValue() {
        CurrencyRepresentation representation = createWithValue(0.0f);
        cache.addNewExchangeRate(representation);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addNewExchangeRateShouldThrowExceptionOnBlankProvider() {
        CurrencyRepresentation representation = createWithProvider(" ");
        cache.addNewExchangeRate(representation);
    }

    @Test(expected = NullPointerException.class)
    public void addNewExchangeRateShouldThrowExceptionOnNullCurrency() {
        CurrencyRepresentation representation = createWithCurrency(null);
        cache.addNewExchangeRate(representation);
    }

    @Test(expected = NullPointerException.class)
    public void addNewExchangeRateShouldThrowExceptionOnNullObtainedAt() {
        CurrencyRepresentation representation = createWithObtainedAt(null);
        cache.addNewExchangeRate(representation);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addNewExchangeRateShouldThrowExceptionOnPLN() {
        CurrencyRepresentation representation = createWithCurrency(Currency.PLN);
        cache.addNewExchangeRate(representation);
    }

    @Test
    public void addNewExchangeRateShouldReturnAddedValue() {
        CurrencyRepresentation representation = createDefault();

        cache.addNewExchangeRate(representation);

        CurrencyRepresentation received = cache.getExchangeRate(CURR, PROVIDER);

        assertThat(received).isEqualTo(representation);
    }

    @Test
    public void addNewExchangeRateShouldReplaceForDifferentValue() {
        CurrencyRepresentation base = createDefault();
        CurrencyRepresentation newValue = createWithValue(VALUE + 3.0f);

        cache.addNewExchangeRate(base);
        cache.addNewExchangeRate(newValue);

        CurrencyRepresentation received = cache.getExchangeRate(CURR, PROVIDER);

        assertThat(received).isEqualTo(newValue);
    }

    @Test
    public void addNewExchangeRateShouldReplaceForDifferentObtainedAt() {
        CurrencyRepresentation base = createDefault();
        CurrencyRepresentation newValue = createWithObtainedAt(OBTAINED_AT.plus(1, ChronoUnit.DAYS));

        cache.addNewExchangeRate(base);
        cache.addNewExchangeRate(newValue);

        CurrencyRepresentation received = cache.getExchangeRate(CURR, PROVIDER);

        assertThat(received).isEqualTo(newValue);
    }

    @Test
    public void addNewExchangeRateShouldReplaceForDifferentValueAndObtainedAt() {
        CurrencyRepresentation base = createDefault();
        CurrencyRepresentation newValue = createWithValueAndObtainedAt(VALUE + 2.0f,
                OBTAINED_AT.plus(1, ChronoUnit.DAYS));

        cache.addNewExchangeRate(base);
        cache.addNewExchangeRate(newValue);

        CurrencyRepresentation received = cache.getExchangeRate(CURR, PROVIDER);

        assertThat(received).isEqualTo(newValue);
    }

    @Test
    public void addNewExchangeRateShouldAddForDifferentProvider() {
        String newProvider = PROVIDER + "2";
        CurrencyRepresentation base = createDefault();
        CurrencyRepresentation newValue = createWithProvider(newProvider);

        cache.addNewExchangeRate(base);
        cache.addNewExchangeRate(newValue);

        CurrencyRepresentation baseProviderRep = cache.getExchangeRate(CURR, PROVIDER);
        CurrencyRepresentation newProviderRep = cache.getExchangeRate(CURR, newProvider);

        assertThat(baseProviderRep).isEqualTo(base);
        assertThat(newProviderRep).isEqualTo(newValue);
    }

    @Test
    public void addNewExchangeRateShouldAddForDifferentCurrency() {
        Currency newCurr = CURR == Currency.EUR ? Currency.GBP : Currency.EUR;
        CurrencyRepresentation base = createDefault();
        CurrencyRepresentation newValue = createWithCurrency(newCurr);

        cache.addNewExchangeRate(base);
        cache.addNewExchangeRate(newValue);

        CurrencyRepresentation baseCurrRep = cache.getExchangeRate(CURR, PROVIDER);
        CurrencyRepresentation newCurrRep = cache.getExchangeRate(newCurr, PROVIDER);

        assertThat(baseCurrRep).isEqualTo(base);
        assertThat(newCurrRep).isEqualTo(newValue);
    }

    @Test
    public void addNewExchangeRateShouldAddForDifferentCurrencyAndProvider() {
        Currency newCurr = CURR == Currency.EUR ? Currency.GBP : Currency.EUR;
        String newProvider = PROVIDER + "2";
        CurrencyRepresentation base = createDefault();
        CurrencyRepresentation newValue = createWithCurrencyAndProvider(newCurr, newProvider);

        cache.addNewExchangeRate(base);
        cache.addNewExchangeRate(newValue);

        CurrencyRepresentation baseRep = cache.getExchangeRate(CURR, PROVIDER);
        CurrencyRepresentation newRep = cache.getExchangeRate(newCurr, newProvider);

        assertThat(baseRep).isEqualTo(base);
        assertThat(newRep).isEqualTo(newValue);
    }

    @Test
    public void getExchangeRateShouldReturnNullIfNotFound() {
        CurrencyRepresentation notExisting = cache.getExchangeRate(CURR, PROVIDER);

        assertThat(notExisting).isNull();
    }

}
