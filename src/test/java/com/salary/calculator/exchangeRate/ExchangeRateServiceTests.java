package com.salary.calculator.exchangeRate;

import com.salary.calculator.exchangeRate.cache.ExchangeRateCacheInterface;
import com.salary.calculator.exchangeRate.provider.ExchangeRateProvider;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static com.salary.calculator.exchangeRate.Currency.EUR;
import static com.salary.calculator.exchangeRate.Currency.PLN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateServiceTests {

    private static final float VALUE = 2.0f;
    private static final float NEW_VALUE = 3.0f;
    private static final String ERROR_MSG = "Error";
    private static final String PROVIDER_1_NAME = "PROVIDER_1";
    private static final String PROVIDER_2_NAME = "PROVIDER_2";
    private static final String NOT_SUPPORTED_PROVIDER_NAME = "PROVIDER_3";
    @Mock
    private ExchangeRateProvider provider1;

    @Mock
    private ExchangeRateProvider provider2;

    @Mock
    private ExchangeRateCacheInterface cache;

    private ExchangeRateService service;

    @Before
    public void setup() {
        service = new ExchangeRateServiceImpl(Arrays.asList(provider1, provider2), cache);
    }

    @Test()
    public void getExchangeRateShouldReturnPLNRepresentationOnPLN() throws GetExchangeRateException {
        CurrencyRepresentation result = service.getExchangeRate(PLN, null, false);

        assertThat(result).isEqualTo(CurrencyRepresentation.createPLNRepresentation());
    }

    @Test(expected = NullPointerException.class)
    public void getExchangeRateShouldThrowOnNoCurrency() throws GetExchangeRateException {
        service.getExchangeRate(null, null, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getExchangeRateShouldThrowOnNotSupportedProvider() throws GetExchangeRateException {
        doReturn(PROVIDER_1_NAME).when(provider1).getProviderName();
        doReturn(PROVIDER_2_NAME).when(provider2).getProviderName();

        service.getExchangeRate(EUR, NOT_SUPPORTED_PROVIDER_NAME, false);
    }

    @Test
    public void getExchangeRateShouldReturnFromCache() throws GetExchangeRateException {
        CurrencyRepresentation rep = new CurrencyRepresentation(VALUE, Instant.now(), EUR, PROVIDER_1_NAME);

        doReturn(rep).when(cache).getExchangeRate(EUR, PROVIDER_1_NAME);
        doReturn(PROVIDER_1_NAME).when(provider1).getProviderName();

        CurrencyRepresentation result = service.getExchangeRate(EUR, PROVIDER_1_NAME, false);

        assertThat(result).isEqualTo(rep);
    }

    @Test
    public void getExchangeRateShouldReturnFromCacheOnNoProviderName() throws GetExchangeRateException {
        CurrencyRepresentation rep = new CurrencyRepresentation(VALUE, Instant.now(), EUR, PROVIDER_1_NAME);

        doReturn(rep).when(cache).getExchangeRate(EUR, PROVIDER_2_NAME);
        doReturn(PROVIDER_1_NAME).when(provider1).getProviderName();
        doReturn(PROVIDER_2_NAME).when(provider2).getProviderName();

        CurrencyRepresentation result = service.getExchangeRate(EUR, null, false);

        assertThat(result).isEqualTo(rep);
    }

    @Test
    public void getExchangeRateShouldReturnedFromProviderOnNoCache() throws GetExchangeRateException {
        CurrencyRepresentation providerRep = new CurrencyRepresentation(NEW_VALUE, Instant.now(), EUR, PROVIDER_1_NAME);

        doReturn(providerRep).when(provider1).getExchangeRate(EUR);
        doReturn(PROVIDER_1_NAME).when(provider1).getProviderName();

        CurrencyRepresentation result = service.getExchangeRate(EUR, PROVIDER_1_NAME, true);

        assertThat(result).isEqualTo(providerRep);
    }

    @Test
    public void getExchangeRateShouldReturnFromProviderOnNoCacheAndNoProviderName() throws GetExchangeRateException {
        CurrencyRepresentation providerRep = new CurrencyRepresentation(NEW_VALUE, Instant.now(), EUR, PROVIDER_1_NAME);

        doReturn(providerRep).when(provider1).getExchangeRate(EUR);

        CurrencyRepresentation result = service.getExchangeRate(EUR, null, true);

        assertThat(result).isEqualTo(providerRep);
    }

    @Test
    public void getExchangeRateShouldReturnFromProviderOnNoCache() throws GetExchangeRateException {
        CurrencyRepresentation providerRep = new CurrencyRepresentation(NEW_VALUE, Instant.now(), EUR, PROVIDER_1_NAME);

        doReturn(providerRep).when(provider1).getExchangeRate(EUR);

        CurrencyRepresentation result = service.getExchangeRate(EUR, null, false);

        assertThat(result).isEqualTo(providerRep);
    }

    @Test(expected = GetExchangeRateException.class)
    public void getExchangeRateShouldThrowOnNoCacheProviderNameErrorDuringProviderCall() throws GetExchangeRateException {
        doThrow(new GetExchangeRateException(EUR, PROVIDER_1_NAME, ERROR_MSG)).when(provider1).getExchangeRate(EUR);
        doReturn(PROVIDER_1_NAME).when(provider1).getProviderName();

        service.getExchangeRate(EUR, PROVIDER_1_NAME, false);
    }

    @Test
    public void getExchangeRateShouldReturnFromProvider2OnProvider1Broken() throws GetExchangeRateException {
        CurrencyRepresentation providerRep = new CurrencyRepresentation(NEW_VALUE, Instant.now(), EUR, PROVIDER_2_NAME);
        doThrow(new GetExchangeRateException(EUR, PROVIDER_1_NAME, ERROR_MSG)).when(provider1).getExchangeRate(EUR);
        doReturn(providerRep).when(provider2).getExchangeRate(EUR);

        CurrencyRepresentation rep = service.getExchangeRate(EUR, null, false);

        assertThat(rep).isEqualTo(providerRep);
    }

    @Test(expected = GetExchangeRateException.class)
    public void getExchangeRateShouldThrowOnAllProvidersBroken() throws GetExchangeRateException {
        doThrow(new GetExchangeRateException(EUR, PROVIDER_1_NAME, ERROR_MSG)).when(provider1).getExchangeRate(EUR);
        doThrow(new GetExchangeRateException(EUR, PROVIDER_2_NAME, ERROR_MSG)).when(provider2).getExchangeRate(EUR);

        service.getExchangeRate(EUR, null, false);
    }

    @Test
    public void getProvidersNamesShouldReturnAllNames() {
        doReturn(PROVIDER_1_NAME).when(provider1).getProviderName();
        doReturn(PROVIDER_2_NAME).when(provider2).getProviderName();

        List<String> providersName = service.getProvidersNames();

        assertThat(providersName.size()).isEqualTo(2);
        assertThat(providersName.contains(PROVIDER_1_NAME)).isTrue();
        assertThat(providersName.contains(PROVIDER_2_NAME)).isTrue();
    }
}
