package com.salary.calculator.exchangeRate.provider.nbp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NBPExchangeRateProviderSerializationTests {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldSerializeAllFields() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("euro_response_nbp.json").getFile());
        NBPExchangeRateResponse response = objectMapper.readValue(file, NBPExchangeRateResponse.class);

        assertThat(response).isNotNull();
        assertThat(response.getCode()).isEqualTo("EUR");
        assertThat(response.getTable()).isEqualTo("A");
        assertThat(response.getCode()).isEqualTo("EUR");
        assertThat(response.getRates().size()).isEqualTo(1);
        assertThat(response.getRates().get(0).getNo()).isEqualTo("130/A/NBP/2019");
        assertThat(response.getRates().get(0).getMid()).isEqualTo(4.2519f);
        assertThat(response.getRates().get(0).getEffectiveDate()).isNotNull();
    }
}
