package com.salary.calculator.exchangeRate.provider.ecb;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ECBExchangeRateProviderSerializationTests {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldSerializeAllFields() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("euro_response_ecb.json").getFile());
        ECBExchangeRateResponse response = objectMapper.readValue(file, ECBExchangeRateResponse.class);

        assertThat(response).isNotNull();
        assertThat(response.getBase()).isEqualTo("EUR");
        assertThat(response.getDate()).isNotNull();
        assertThat(response.getRates().size()).isEqualTo(32);
        assertThat(response.getRates().get("PLN")).isEqualTo(4.253f);
    }
}
