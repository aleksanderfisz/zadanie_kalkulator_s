package com.salary.calculator.exchangeRate.provider.nbp;

import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Date;

import static com.salary.calculator.exchangeRate.Currency.EUR;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NBPExchangeRateProviderTests {
    private static final float MID = 2.0f;
    private static final String NO = "10";
    private static final String TABLE = "A";
    private static final String CURRENCY = "Euro";
    private static final String ERROR_MSG = "Error";

    @Mock
    private RestTemplate restTemplate;

    private NBPExchangeRateProvider provider;

    @Before
    public void setup() {
        provider = new NBPExchangeRateProvider(restTemplate);
    }

    @Test
    public void getExchangeRateShouldReturnCurrencyRepresentation() throws GetExchangeRateException {
        NBPRate rate = new NBPRate(NO, new Date(), MID);
        NBPExchangeRateResponse rateResponse = new NBPExchangeRateResponse(TABLE, CURRENCY, EUR.name(),
                Collections.singletonList(rate));
        ResponseEntity<NBPExchangeRateResponse> resp = new ResponseEntity<>(rateResponse, HttpStatus.OK);

        when(restTemplate.getForEntity(provider.getNbpRatesUrl(EUR), NBPExchangeRateResponse.class)).thenReturn(resp);

        CurrencyRepresentation rep = provider.getExchangeRate(EUR);

        assertThat(rep.getCurr()).isEqualTo(EUR);
        assertThat(rep.getValue()).isEqualTo(MID);
    }

    @Test(expected = GetExchangeRateException.class)
    public void getExchangeRateShouldThrowForInvalidResponseCode() throws GetExchangeRateException {
        ResponseEntity<NBPExchangeRateResponse> resp = new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        when(restTemplate.getForEntity(provider.getNbpRatesUrl(EUR), NBPExchangeRateResponse.class)).thenReturn(resp);

        provider.getExchangeRate(EUR);
    }

    @Test(expected = GetExchangeRateException.class)
    public void getExchangeRateShouldThrowForErrorDuringCall() throws GetExchangeRateException {
        when(restTemplate.getForEntity(provider.getNbpRatesUrl(EUR), NBPExchangeRateResponse.class))
                .thenThrow(new ResourceAccessException(ERROR_MSG));

        provider.getExchangeRate(EUR);
    }

    @Test(expected = GetExchangeRateException.class)
    public void getExchangeRateShouldThrowForErrorEmptyRates() throws GetExchangeRateException {
        NBPExchangeRateResponse rateResponse = new NBPExchangeRateResponse(TABLE, CURRENCY, EUR.name(),
                null);
        ResponseEntity<NBPExchangeRateResponse> resp = new ResponseEntity<>(rateResponse, HttpStatus.OK);

        when(restTemplate.getForEntity(provider.getNbpRatesUrl(EUR), NBPExchangeRateResponse.class)).thenReturn(resp);

        provider.getExchangeRate(EUR);
    }

}
