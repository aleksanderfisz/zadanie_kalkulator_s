package com.salary.calculator.exchangeRate.provider.ecb;

import com.salary.calculator.exchangeRate.CurrencyRepresentation;
import com.salary.calculator.exchangeRate.provider.GetExchangeRateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.salary.calculator.exchangeRate.Currency.EUR;
import static com.salary.calculator.exchangeRate.Currency.PLN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ECBExchangeRateProviderTests {

    private static final float VALUE = 2.0f;
    private static final String ERROR_MSG = "Error";

    @Mock
    private RestTemplate restTemplate;

    private ECBExchangeRateProvider provider;

    @Before
    public void setup() {
        provider = new ECBExchangeRateProvider(restTemplate);
    }


    @Test
    public void getExchangeRateShouldReturnCurrencyRepresentation() throws GetExchangeRateException {
        Map<String, Float> rates = new HashMap<>();
        rates.put(PLN.name(), VALUE);
        ECBExchangeRateResponse resp = new ECBExchangeRateResponse(EUR.name(), new Date(), rates);
        ResponseEntity<ECBExchangeRateResponse> respEnt = new ResponseEntity<>(resp, HttpStatus.OK);

        when(restTemplate.getForEntity(provider.getECBRatesUrl(EUR), ECBExchangeRateResponse.class)).thenReturn(respEnt);

        CurrencyRepresentation rep = provider.getExchangeRate(EUR);

        assertThat(rep.getCurr()).isEqualTo(EUR);
        assertThat(rep.getValue()).isEqualTo(VALUE);
    }

    @Test(expected = GetExchangeRateException.class)
    public void getExchangeRateShouldThrowForInvalidResponseCode() throws GetExchangeRateException {
        ResponseEntity<ECBExchangeRateResponse> resp = new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        when(restTemplate.getForEntity(provider.getECBRatesUrl(EUR), ECBExchangeRateResponse.class)).thenReturn(resp);

        provider.getExchangeRate(EUR);
    }

    @Test(expected = GetExchangeRateException.class)
    public void getExchangeRateShouldThrowForErrorDuringCall() throws GetExchangeRateException {
        when(restTemplate.getForEntity(provider.getECBRatesUrl(EUR), ECBExchangeRateResponse.class))
                .thenThrow(new ResourceAccessException(ERROR_MSG));

        provider.getExchangeRate(EUR);
    }

    @Test(expected = GetExchangeRateException.class)
    public void getExchangeRateShouldThrowForEmptyRates() throws GetExchangeRateException {
        ECBExchangeRateResponse resp = new ECBExchangeRateResponse(EUR.name(), new Date(), null);
        ResponseEntity<ECBExchangeRateResponse> respEnt = new ResponseEntity<>(resp, HttpStatus.OK);

        when(restTemplate.getForEntity(provider.getECBRatesUrl(EUR), ECBExchangeRateResponse.class)).thenReturn(respEnt);

        provider.getExchangeRate(EUR);
    }

}
