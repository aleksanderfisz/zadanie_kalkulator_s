package com.salary.calculator.country;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ComponentScan(basePackageClasses = CountryResources.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CountryResourcesTests {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getAllCountriesShouldReturnAllCountries() throws Exception {
        mvc.perform(get("/country/getAll"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(Country.values().length)))
                .andExpect(jsonPath("$[0]", is(Country.values()[0].name())));
    }
}
