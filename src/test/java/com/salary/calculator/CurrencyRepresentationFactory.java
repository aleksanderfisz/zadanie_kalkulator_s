package com.salary.calculator;

import com.salary.calculator.exchangeRate.Currency;
import com.salary.calculator.exchangeRate.CurrencyRepresentation;

import java.time.Instant;

public class CurrencyRepresentationFactory {

    public static final Currency CURR = Currency.EUR;
    public static final String PROVIDER = "Test Provider";
    public static final Instant OBTAINED_AT = Instant.now();
    public static final float VALUE = 2.0f;

    private CurrencyRepresentationFactory() {
    }

    public static CurrencyRepresentation createWithValue(float value) {
        return CurrencyRepresentation.builder()
                .value(value)
                .obtainedAt(OBTAINED_AT)
                .provider(PROVIDER)
                .curr(CURR)
                .build();
    }

    public static CurrencyRepresentation createWithProvider(String provider) {
        return CurrencyRepresentation.builder()
                .provider(provider)
                .obtainedAt(OBTAINED_AT)
                .value(VALUE)
                .curr(CURR)
                .build();
    }

    public static CurrencyRepresentation createWithCurrency(Currency curr) {
        return CurrencyRepresentation.builder()
                .curr(curr)
                .obtainedAt(OBTAINED_AT)
                .value(VALUE)
                .provider(PROVIDER)
                .build();
    }

    public static CurrencyRepresentation createWithObtainedAt(Instant obtainedAt) {
        return CurrencyRepresentation.builder()
                .obtainedAt(obtainedAt)
                .value(VALUE)
                .curr(CURR)
                .provider(PROVIDER)
                .build();
    }

    public static CurrencyRepresentation createWithValueAndObtainedAt(float value, Instant obtainedAt) {
        return CurrencyRepresentation.builder()
                .value(value)
                .obtainedAt(obtainedAt)
                .curr(CURR)
                .provider(PROVIDER)
                .build();
    }

    public static CurrencyRepresentation createWithCurrencyAndProvider(Currency currency, String provider) {
        return CurrencyRepresentation.builder()
                .curr(currency)
                .provider(provider)
                .value(VALUE)
                .obtainedAt(OBTAINED_AT)
                .build();
    }

    public static CurrencyRepresentation createDefault() {
        return CurrencyRepresentation.builder()
                .value(VALUE)
                .curr(CURR)
                .provider(PROVIDER)
                .obtainedAt(OBTAINED_AT)
                .build();
    }

}
