# NET SALARY CALCULATOR

## Description
Net salary calculator can be used to calculate total net salary.\
Application assumptions:
* month has **22** working days
* countries have fixed cost in their currency
  - Poland **1200**
  - United Kingdom **600**
  - Germany **800**
* countries have flat tax rate
  - Poland **19%**
  - United Kingdom **25%**
  - Germany **20%**
* fixed cost is **not taxed**
* salary can be negative if fixed cost exceeds total gross income

## Currency
To provide greater availability of service application uses two exchange rate providers.\
Application contains exchange rate cache with cache provider to farther increase availability.\
Cache provider calls all providers for exchange rates once per minute.\
In case of provider inaccessibility data still can be retried from cache.

## Build
To build application go to root folder (where mvnw file is located):
* Windows:
> `mvnw.cmd clean package`
* Linux:
> `./mvnw clean package`
 
## Run
To run application go to root folder:

> `java -jar target/salary_calculator.jar`

Application can be accessed at <http://localhost:8080/>

## Configuration
* Configure cache provider
  - create file application.properties
  - to run command add `--spring.config.location=-Dspring.config.location="file:(application.properties apsolute path)"`
  - to disable provider add exchange.rate.cache.enable=false to application properties
  - to change frequency of calling exchange rate providers providers 
  exchange.rate.cache.refresh.second to application.properties
  - to change add custom logback config add `logging.config = file:(location to loging lile)` to application.properties